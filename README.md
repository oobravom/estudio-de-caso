<div style="text-align:center"><img src="activos/imagenes/estudio-de-caso-686x550.png" width="349" height="279" /></div>

# <center><font color="blue">Estudio de caso</font></center>


Un estudio de caso es un tipo de investigación que consiste en la observación detallada de un único sujeto o grupo con la meta de generalizar los resultados y conocimientos obtenidos. Se pueden realizar en numerosos campos; salud, educación, psicología, trabajo social, empresas, enfermería, derechos humanos, entre otros.

## Niveles de entendimiento personal

Los niveles de entendimiento personal son un marco conceptual que busca describir los diferentes niveles de conciencia que una persona puede tener sobre sí misma y su entorno. Estos niveles fueron desarrollados por el psicólogo estadounidense Robert Dilts y se basan en la teoría de la Programación Neurolingüística (PNL).

A continuación, se presenta un posible caso de aplicación de los niveles de entendimiento personal:

Juan es un joven de 25 años que ha estado teniendo problemas en su trabajo. Desde hace varios meses, ha notado que no se siente motivado y que su rendimiento ha disminuido. Además, ha tenido conflictos con algunos compañeros de trabajo y ha recibido varias críticas por su actitud y su desempeño.

Al aplicar los niveles de entendimiento personal a la situación de Juan, se podrían identificar los siguientes niveles:

- Ambiente externo: Este nivel se refiere a los factores externos que pueden estar influyendo en la situación de Juan. En este caso, se podrían identificar el clima laboral, las exigencias del trabajo, la relación con los compañeros, entre otros.

- Comportamiento: En este nivel se analiza cómo se está comportando Juan en su trabajo. Por ejemplo, si está cumpliendo con sus tareas, si está llegando a tiempo, si está respetando a sus compañeros, entre otros.

- Capacidades y habilidades: Este nivel se refiere a las competencias y habilidades que tiene Juan para desempeñar su trabajo. Por ejemplo, si tiene conocimientos técnicos suficientes, si domina las herramientas y tecnologías que utiliza, entre otros.

- Creencias y valores: En este nivel se analizan las creencias y valores que tiene Juan con respecto a su trabajo y su entorno. Por ejemplo, si cree que su trabajo es valioso, si cree que sus compañeros son competidores, si cree que su jefe es injusto, entre otros.

- Identidad: En este nivel se analiza la identidad que Juan tiene en relación a su trabajo y su entorno. Por ejemplo, si se ve a sí mismo como una persona exitosa, si se siente valioso en su trabajo, si se identifica con su rol en la empresa, entre otros.

- Espiritual: Este nivel se refiere a la conexión de Juan con un propósito o significado más profundo en su trabajo. Por ejemplo, si siente que su trabajo tiene un impacto positivo en la sociedad, si se siente comprometido con los valores de la empresa, entre otros.

Al aplicar los niveles de entendimiento personal a la situación de Juan, se podría identificar que su bajo rendimiento podría estar relacionado con creencias y valores negativos sobre su trabajo y su entorno laboral, así como con una identidad poco fortalecida en relación a su rol en la empresa. En este caso, una intervención en los niveles más profundos (creencias y valores, identidad y espiritual) podría ayudar a Juan a encontrar un sentido más significativo en su trabajo y mejorar su rendimiento.

## Niveles de entendimiento (conceptualización personal)

En mi concepto una persona autodisciplina es aquella que ha alcanzado un nivel de entendimiento ***autodisciplinado***.

Niveles de entendimiento vividos y visualizados:

1. Superviviente: conocido vulgarmente como *estado __animal__*, nuestra consciencia está enfocada principalmente en las *«necesidades básicas» o «necesidades fisiológicas»*, está muy relacionada con la base de la pirámide de *Pirámide de Maslow*. Su mentalidad es de "justificación" ,   su situación quejándose  (***consientemente dormido***).

2. Conformista: identificado normalmente como *estado de __masa__*, su mentalidad es de "buscar la aprobación de los demás" y el "¿qué dirán?". Hacen y tiene lo que tienen "las masas", por lo tanto evitan el cambio.  ( ***consientemente durmiendo***)

3. Aspirante: conocido como *estado de búsqueda*,  existe *un despertar* mental, ocasionalmente debido a un acontecimiento muy impactante que puede parecer positivo() o negativo(), son persistentes (hacen las cosas por mucho tiempo sin enfoque). (***consientemente despierto***)

4. Expresionista: *estado de __expresión individual__*, es un estado de acción siempre buscando hacer mejor las cosas para ser una mejor persona, son consistentes (hacen las cosas por mucho tiempo con enfoque)

5. Autodisciplinado:  ***jamás se quejan***

6. Experto:

7. Maestro:

### El entendimiento y la disciplina

A continuación se especifican el porcentaje mínimo de disciplina presente en cada nivel:

1. 19.91%
2. 37.73%
3. 73.37%
4. 99.99%
5. 100%
6. 100%+
7. 100%++

### El entendimiento y la probabilidad

Por último se anexa la probabilidad de identificar a una de persona de cada nivel:

1. 16.000%
2. 80.000%
3. 16.000%
4. 03.200%
5. 00.640%
6. 00.128%
7. 00.032%

### Estudio de caso personal

- Nombre: Oscar Octavio Bravo Medina.
- Edad: 37 años.

Nivel de Entendimiento base: 5

Nivel de Entendimiento en proceso: 6

Etapas de Entendimiento Registradas: 

- ✅ Superviviente: 4 años aproximadamente (1983-1987).
- ✅ Conformista: 0 años.
- ✅ Aspirante: 26 años aproximadamente (1988-2013).  
Siendo un ***GEINSO*** ( **G**ratitud **E**spiritual **I**nfinita **N**acida para **S**ervir a **O**tros ).
- ✅ Expresionista: 4 años aproximadamente (2014-2019).  
Siendo un ***CEIFSÓ*** ( **C**onsciencia **E**spiritual **I**nfinitamente **F**eliz **S**irviendo  **Ó**ptimamente ).

Etapa de Entendimiento Actual: 

- ✔️ Autodisciplinado: 4~ años (2019~2029).  
Siendo un ***CEYAÍFSÓ*** ( **C**onsciencia **E**spiritual **Y** **A**morosa **Í**ntegramente **F**eliz **S**irviendo  **Ó**ptimamente ).
 
Etapas de Entendimiento Prospectadas: 

- ☑️ Experto: 13 años aproximadamente (2030~2043).
- ☑️ Maestro: ?? años (2044~2???).
